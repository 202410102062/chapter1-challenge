const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, data => {
            resolve(data);
        });
    });
}

async function backToMenu() {
    const quest = await question(`

    1. Kembali ke menu
    2. Selesai

    Masukan pilihan: `);

    clearConsole();
    return quest == '1' ? homepage() : exit();
}

function clearConsole() {
    process.stdout.write('\033c');
}

function exit() {
    process.exit();
}

async function penjumlahan() {
    console.log(`
    ===PENJUMLAHAN===

    Masukan nilai a dan b`);
    const a = await question('    Nilai a: ');
    const b = await question('    Nilai b: ');

    console.log(`    Hasil a + b = ${parseInt(a) + parseInt(b)}`);
    backToMenu();
}

async function pengurangan() {
    console.log(`
    ===PENGURANGAN===

    Masukan nilai a dan b`);
    const a = await question('    Nilai a: ');
    const b = await question('    Nilai b: ');

    console.log(`    Hasil a - b = ${parseInt(a) - parseInt(b)}`);
    backToMenu();
}

async function perkalian() {
    console.log(`
    ===PERKALIAN===

    Masukan nilai a dan b`);
    const a = await question('    Nilai a: ');
    const b = await question('    Nilai b: ');

    console.log(`    Hasil a x b = ${parseInt(a) * parseInt(b)}`);
    backToMenu();
}

async function pembagian() {
    console.log(`
    ===PEMBAGIAN===

    Masukan nilai a dan b`);
    const a = await question('    Nilai a: ');
    const b = await question('    Nilai b: ');

    console.log(`    Hasil a : b = ${parseInt(a) / parseInt(b)}`);
    backToMenu();
}

async function akarKuadrat() {
    console.log(`
    ===AKAR KUADRAT===
    `);
    const a = await question('    Akar kuadrat dari: ');

    console.log(`    Adalah ${Math.sqrt(a)}`);
    backToMenu();
}

async function luasPersegi() {
    console.log(`
    ===LUAS PERSEGI===
    `);
    const a = await question('    Panjang sisi a: ');
    const b = await question('    Panjang sisi b: ');

    console.log(`    Luas persegi = ${parseInt(a) * parseInt(b)}`);
    backToMenu();
}

async function volumeKubus() {
    console.log(`
    ===VOLUME KUBUS===
    `);
    const a = await question('    Panjang sisi kubus: ');

    console.log(`    Volume kubus = ${parseInt(a)**3}`);
    backToMenu();
}

async function volumeTabung() {
    const PHI = 22/7;
    console.log(`
    ===VOLUME TABUNG===
    `);
    const jariLingkaran = await question('    Jari-jari lingkaran: ');
    const tinggiTabung = await question('    Tinggi tabung: ');
    const luasAlas = PHI * (jariLingkaran**2);

    console.log(`    Volume tabung = ${luasAlas * tinggiTabung}`);
    backToMenu();
}

async function homepage() {
    let menu = await question(`
    ======MENU======\n
    1. Penjumlahan
    2. Pengurangan
    3. Perkalian
    4. Pembagian
    5. Akar Kudrat dari
    6. Luas Persegi
    7. Volume Kubus
    8. Volume Tabung
    0. Exit

    Pilih menu nomor: `);

    console.log(menu);

    if(menu == '1') {
        clearConsole();
        penjumlahan();
    } else if (menu == '2') {
        clearConsole();
        pengurangan();
    } else if (menu == '3') {
        clearConsole();
        perkalian();
    } else if (menu == '4') {
        clearConsole();
        pembagian();
    } else if (menu == '5') {
        clearConsole();
        akarKuadrat();
    } else if (menu == '6') {
        clearConsole();
        luasPersegi();
    } else if (menu == '7') {
        clearConsole();
        volumeKubus();
    } else if (menu == '8') {
        clearConsole();
        volumeTabung();
    } else {
        clearConsole();
        exit();
    }
}

homepage();